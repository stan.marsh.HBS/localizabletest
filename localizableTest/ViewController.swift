//
//  ViewController.swift
//  localizableTest
//
//  Created by stan marsh on 2019/2/20.
//  Copyright © 2019 stan marsh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var programText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        programText.text = NSLocalizedString("ProgramText", comment: "")
    }


}

